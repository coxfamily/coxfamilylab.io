import React from "react"
//import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="CoxFamily" />
    <h1>Howdy people!</h1>
    <p>Welcome to the CoxFamily gitlab.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <p> The big <a href="http://xmas2019.mcquilter.com/">xmas2019 photo album</a>.</p>
    <p> The walkthrough of the Steve and Tosha Casa
      <iframe title="Steve and Tosha Casa Walkthrough" width="640" height="360" scrolling="no" frameborder="0" src="https://www.bitchute.com/embed/7pMwA6FKezwF/"></iframe>
    </p>
    <p> Coriolan Copper
      <iframe title="Coriolan Copper" width="640" height="360" scrolling="no" frameborder="0" src="https://www.bitchute.com/embed/67DnGGuknFct/"></iframe>
    </p>
  </Layout>
)

export default IndexPage
