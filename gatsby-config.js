module.exports = {
  siteMetadata: {
    title: `CoxFamily`,
    description: `This site will be the focal point for our CoxFamily web presence.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `CoxFamily.gitlab.io`,
        short_name: `CoxFamily`,
        start_url: `/`,
        background_color: `#663333`,
        theme_color: `#663333`,
        display: `minimal-ui`,
        icon: `src/images/CoxFamily.jpg`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
